import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware, { END } from 'redux-saga';
import logger from 'redux-logger';

import rootReducer from '../reducers';
import rootSaga from '../sagas';

// Config the global state store and connect instances from all middleware, such as saga, to the store.
export default function configStore(initialState) {
    const sagaMiddleware = createSagaMiddleware();

    const store = createStore(
        rootReducer,
        initialState,
        compose(
            applyMiddleware(
                sagaMiddleware, logger
            )
        )
    );

    sagaMiddleware.run(rootSaga);
    store.close = () => store.dispatch(END);
    return store;
}
