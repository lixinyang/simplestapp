import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import * as loginActions from '../actions/loginActions';
import LoginComponent from '../components/login/LoginComponent';

// Wrapp LoginComponent into a LoginContainer
const LoginContainer = ({
  ...props
}) => (<LoginComponent {... props}/>);

// response: Server Side or Mock > response > Saga > responsAction > MQ > Reducer > Global State Store > props > Container > Component > componentWillReceiveProps(nextProps)
// Map global state or its partitions (named after the corresponding reducer) when a change happens in the store
function mapGlobalStateToProps(globalState) {
  // Return object which includes all state values in the loginReducer partition of the global state store
  return {
    loginReducer: globalState.loginReducer,
    //eg. anotherPartition: globalState.anotherPartition,
  }; 
}

// request: Component > this.props.***Action(request) > MQ > Saga > Server Side or Mock
// Map actions to props which are passed into the component so that the component can call actions in a way like 
// "this.props.someAction()". Such a call will push an action into the message queue
function mapActionsToProps(dispatch) {
  return bindActionCreators(loginActions, dispatch);
}

// Attach the above mappings to the component which is wrapped in this container
export default connect(mapGlobalStateToProps, mapActionsToProps)(LoginContainer);
