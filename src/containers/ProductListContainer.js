import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as productActions from '../actions/productActions';
import ProductListComponent from '../components/products/ProductListComponent';

const ProductListContainer = ({...props}) => (<ProductListComponent {... props}/>);

function mapStateToProps(state) {
    return {
        productReducer: state.productReducer
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(productActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductListContainer);
