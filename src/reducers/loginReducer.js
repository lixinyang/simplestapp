import * as actionTypes from '../constants/actions';

export default function loginReducer(state = {
        loginRequest: null,
        loginResponseMap: null
    } , action) {
    switch (action.type) {
        // Construct of login request action is optional as the request is only required by and 
        // passed to saga via a custom MQ instead of sensing the store.
        /*case actionTypes.LOGIN_REQUEST:
            return {
                ...state,
                loginRequest: action.loginRequest
            }
            break;*/
        // Construct of login response action which is passed back from the saga to the component
        case actionTypes.LOGIN_RESPONSE:
            return {
                ...state,
                loginResponseMap: action.loginResponseMap
            }
            break;
        default:
            return state;
    }
}