import {combineReducers} from 'redux';

import loginReducer from './loginReducer';

// Combine all individual reducers
export default combineReducers({loginReducer});