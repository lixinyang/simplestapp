import * as actionTypes from '../constants/actions';

/// 
export default function anotherReducer(anotherPatition = {
        anotherRequest, anotherResponse
    } , action) {
    switch (action.type) {
        /* case actionTypes.ANOTHER_REQUEST:
            return {
                ...anotherPatition,
                anotherRequest: action.anotherRequest
            }
            break; */
        case actionTypes.ANOTHER_RESPONSE:
            return {
                ...anotherPatition,
                anotherResponse: action.anotherResponse
            }
            break;
        case actionTypes.ANOTHER_MESSAGE:
            return {
                ...anotherPatition,
                anotherMessage: action.anotherMessage
            }
            break;
        default:
            return anotherPatition;
    }
}