import { fork, all } from 'redux-saga/effects';

import loginSaga from './loginSaga';

// Start all the saga instances
export default function* rootSaga() {
  yield all([
    fork(loginSaga)
  ]);
}
