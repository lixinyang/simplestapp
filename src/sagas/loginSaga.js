import { delay } from 'redux-saga';
import { fork, cancel, call, put, race, take, takeLatest } from 'redux-saga/effects';

import * as actionTypes from '../constants/actions';
import * as loginActions from '../actions/loginActions';

function* login() {
    while (true) {
        const requestAction = yield take(actionTypes.LOGIN_REQUEST);
        console.log(`Received request at ${new Date()}`);
        const response = yield call(mockLogin, requestAction.loginRequest);
        const responseMapAction = loginActions.loginResponseMapAction(response);
        yield put(responseMapAction);
    }
}

export default function* loginSaga() {
    yield [fork(login)];
}

function mockLogin(loginRequest) {
    return {
        succeeded: loginRequest.username === 'a' && loginRequest.password === '1'
    };
}
