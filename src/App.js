import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';
import registerScreens from './screens';
import configStore from './store/configStore';

// Register navigatable screens with global state store
registerScreens(configStore(), Provider);

// App class for initialiting a tab based UI
export default class App {
    static startApp() {
        Navigation.startTabBasedApp({
            tabs: [
                {
                    label: 'Home',
                    screen: 'global.Login',
                    icon: require('./common/Images/compass_gray.png'),
                    selectedIcon: require('./common/Images/compass_active.png'),
                    navigatorStyle: {
                        navBarHidden: true,
                        tabBarHidden: true
                    }
                }
            ],
            tabsStyle: {
                tabBarTextFontFamily: 'Arial',
                tabBarSelectedButtonColor: '#ffff00',
                tabBarBackgroundColor: '#fff'
            },
            appStyle: {
                tabBarBackgroundColor: '#fff',
                tabBarButtonColor: '#d0d0d0',
                navBarTextFontSize: 34,
                forceTitlesDisplay: true,
                tabFontFamily: 'Arial',
                tabBarSelectedButtonColor: '#a0a0a0',
                topBarElevationShadowEnabled: true
            },
            passProps: {},
            animationType: 'fade' 
        });
    }
}
