import React, { PureComponent} from 'react';

import PropTypes from 'prop-types';

import { TouchableOpacity, TouchableNativeFeedback, Platform, Text, View, ViewPropTypes, StyleSheet} from 'react-native';

import { debounce } from 'lodash';
import COMMON_STYLE from './CommonStyle';

const {fonts} = COMMON_STYLE;

const Styles = StyleSheet.create({
  button: {
    marginTop: 20,
    height: 50,
    flexDirection: 'row',
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    ...fonts.medium14,
    color: '#fff',
  },
  border: {
    borderColor: '#fff',
    borderWidth: 1,
    borderRadius: 4
  }
});

export default class Button extends PureComponent {

  static propTypes = {
    text: PropTypes.string,
    buttonStyle: ViewPropTypes.style,
    textStyle: Text.propTypes.style,
    onPress: PropTypes.func,
    noBorder: PropTypes.bool
  };

  static defaultProps = {
    text: null,
    buttonStyle: null,
    textStyle: null,
    onPress: null,
    noBorder: false
  };

  constructor(props) {
    super(props);
    this.onPress = this.onPress.bind(this);
  }

  onPress() {
    this.props.onPress();
  }

  render() {
    const {textStyle, noBorder, buttonStyle, text} = this.props;
    return (
    Platform.OS === 'ios' ?
      <TouchableOpacity style={ [Styles.button, !noBorder && Styles.border, buttonStyle] } onPress={ debounce(this.onPress, 300, {
                                                                                                 leading: true,
                                                                                                 trailing: false
                                                                                               }) }>
        <Text style={ [Styles.text, textStyle] }>
          { text }
        </Text>
      </TouchableOpacity>
      :
      <TouchableNativeFeedback onPress={ debounce(this.onPress, 300, {
                                     leading: true,
                                     trailing: false
                                   }) }>
        <View style={ [Styles.button, !noBorder && Styles.border, buttonStyle] }>
          <Text style={ [Styles.text, textStyle] }>
            { text }
          </Text>
        </View>
      </TouchableNativeFeedback>
    );
  }
}
