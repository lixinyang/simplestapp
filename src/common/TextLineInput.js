import React, { Component } from 'react';

import { Animated, View, Text, TextInput, ViewPropTypes, StyleSheet } from 'react-native';

import COMMON_STYLE from './CommonStyle';

const {THEME_COLOR, tooltipTitle, textField} = COMMON_STYLE;

const textFieldStyle = StyleSheet.flatten(textField);

const Styles = StyleSheet.create({
    wrapper: {
        height: 40
    },
    inputStyle: {
        ...textFieldStyle,
        height: 45,
        marginTop: 5,
        paddingBottom: 0,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: THEME_COLOR.lightGrey
    },
    toolTipWrapper: {
        flexDirection: 'row',
        flex: 1,
        height: 20,
        justifyContent: 'space-between'
    },
    labelText: {
        ...textFieldStyle,
        textAlign: 'left',
        alignSelf: 'flex-start',
        position: 'absolute',
        includeFontPadding: false,
        color: THEME_COLOR.greyish,
        top: 25,
        left: 4,
    }
});


export default class TextLineInput extends Component {

    static defaultProps = {
        placeholder: undefined,
        maxLength: 50,
        minLength: 3,
        showToolTips: true,
        onBlur: undefined,
        returnKeyType: 'done',
        onSubmitEditing: undefined,
        onFocus: undefined,
        style: undefined,
        keyboardType: 'default',
        label: '',
        offsetX: undefined
    };

    static renderToolTipsLeft(minLength, value) {
        const content = (minLength > 0 && value.length < minLength) ? `minimum input ${minLength}` : null;

        return (
            <Text style={ Styles.tooltipTitle }>
              { content }
            </Text>
            );
    }

    constructor(props) {
        super(props);
        this.state = {
            value: '',
            length: 0,
            offsetMaxLength: props.maxLength,
            opacity: new Animated.Value(0),
            translateY: new Animated.Value(0),
            translateX: new Animated.Value(0),
            scale: new Animated.Value(1),
            focuesed: false,
            offsetX: props.offsetX
        };

        this.onChangeText = this.onChangeText.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.onFocus = this.onFocus.bind(this);
        this.focusHandler = this.focusHandler.bind(this);
    }

    onChangeText(value) {
        const length = value.replace(/\s+/g, '').length;
        const offset = value.length - length;

        if (length <= this.props.maxLength) {
            const {maxLength} = this.props;
            this.setState({
                value,
                length,
                offsetMaxLength: maxLength + offset
            });
        }
    }

    onBlur() {
        const {showToolTips, onBlur} = this.props;

        this.setState({
            focuesed: false
        });

        if (showToolTips && !this.animating) {
            const {translateY, translateX, scale, opacity} = this.state;
            if (this.state.value.length === 0) {
                Animated.parallel([
                    Animated.timing(translateY, {
                        toValue: 0,
                        duration: 100,
                        useNativeDriver: true
                    }),
                    Animated.timing(translateX, {
                        toValue: 0,
                        duration: 100,
                        useNativeDriver: true
                    }),
                    Animated.timing(scale, {
                        toValue: 1,
                        duration: 100,
                        useNativeDriver: true
                    }),
                    Animated.timing(opacity, {
                        toValue: 0,
                        duration: 100,
                        useNativeDriver: true
                    })]).start(() => (this.animating = false));
            } else {
                Animated.timing(opacity, {
                    toValue: 0,
                    duration: 100,
                    useNativeDriver: true
                }).start(() => (this.animating = false));
            }
        }

        if (onBlur) {
            onBlur();
        }
    }

    onFocus() {
        const {showToolTips, onFocus} = this.props;

        this.setState({
            focuesed: true
        });

        if (showToolTips && !this.animating) {
            const {translateY, translateX, scale, opacity, offsetX} = this.state;
            Animated.parallel([
                Animated.timing(translateY, {
                    toValue: -25,
                    duration: 100,
                    useNativeDriver: true
                }),
                Animated.timing(translateX, {
                    toValue: -offsetX || -5,
                    duration: 100,
                    useNativeDriver: true
                }),
                Animated.timing(scale, {
                    toValue: 0.86,
                    duration: 100,
                    useNativeDriver: true
                }),
                Animated.timing(opacity, {
                    toValue: 1,
                    duration: 100,
                    useNativeDriver: true
                })]).start(() => (this.animating = false));
        }

        if (onFocus) {
            onFocus();
        }
    }


    /**
     * Press on the floating label or placeholder also result in the input focues
     * @memberof TextLineInput
     */
    focusHandler() {
        if (this.textInput !== null) {
            this.textInput.focus();
        }
    }

    animating = false;
    textInput = null;
    wrapper = null;

    render() {
        const {value, translateY, translateX, scale, opacity, focuesed, length, offsetMaxLength} = this.state;

        const {placeholder, showToolTips, minLength, maxLength, returnKeyType, onSubmitEditing, style, label, keyboardType} = this.props;

        const placeholderText = focuesed ? placeholder : null;

        return (
            <View ref={ ref => (this.wrapper = ref) } style={ [Styles.wrapper, style] }>
              <TextInput ref={ (ref) => (this.textInput = ref) } keyboardType={ keyboardType } maxLength={ offsetMaxLength } onSubmitEditing={ onSubmitEditing } blurOnSubmit={ false }
                onBlur={ this.onBlur } value={ value } returnKeyType={ returnKeyType } onChangeText={ this.onChangeText } onFocus={ this.onFocus } underlineColorAndroid="transparent"
                selectionColor={ THEME_COLOR.color } style={ Styles.inputStyle } placeholder={ placeholderText } placeholderTextColor={ THEME_COLOR.greyish } />
              <Animated.Text style={ [
 Styles.labelText,
 {
 transform: [{ translateX }, { translateY }, { scale }]
 }
 ] } onPress={ this.focusHandler }>
                { label }
              </Animated.Text>
              { showToolTips
                &&
                <Animated.View style={ [Styles.toolTipWrapper, { opacity }] }>
                  { TextLineInput.renderToolTipsLeft(minLength, value) }
                  <Text style={ [Styles.tooltipTitle, Styles.tooltipRight] }>
                    { length }/
                    { maxLength }
                  </Text>
                </Animated.View> }
            </View>
            );
    }
}
