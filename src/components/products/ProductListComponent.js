import React, { PureComponent } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';

import { List } from 'immutable';

import Button from '../../common/Button';

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent',
        flexDirection: 'column',
    },
    bigText: {
        fontSize: 28,
        color: 'orange',
    },
    panel: {
        marginTop: 32
    },
    textLineInput: {
        height: 60
    },
    message: {
        color: 'red',
        marginTop: 20,
        fontSize: 16,
    },
    button: {
        backgroundColor: 'orange',
        paddingLeft: 20,
        paddingRight: 20,
        height: 40
    },
    buttonText: {
        color: 'white',
    }
});

export default class ProductListComponent extends PureComponent {
    render() {
        return (
            <View style={ [Styles.container] }>
              <Text style={ [Styles.bigText] }>
                Product List
              </Text>
            </View>
        )
    }
}