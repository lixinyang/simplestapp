import React, { PureComponent } from 'react';
import { StyleSheet, View, Text, Image, TextInput, ScrollView, Dimensions, Keyboard, KeyboardAvoidingView } from 'react-native';

import { List } from 'immutable';

import Button from '../../common/Button';
import TextLineInput from '../../common/TextLineInput';
var {height, width} = Dimensions.get('window');
const Styles = StyleSheet.create({
    container: {
        flex: 1
    },

    form: {
        height: height - 30,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    appTitle: {
        fontSize: 28,
        color: 'orange',
    },
    panel: {
        marginTop: 20
    },
    textLineInput: {
        height: 60
    },
    message: {
        color: 'red',
        marginTop: 20,
        fontSize: 16,
    },
    button: {
        backgroundColor: 'orange',
        paddingLeft: 20,
        paddingRight: 20,
        height: 40
    },
    buttonText: {
        color: 'white',
    }
});


export default class LoginComponent extends PureComponent {
    state = {
        ...this.props.loginReducer,
        isMessageVisible: false,
        loggedIn: false,
        message: 'warning',
        username: '',
        password: ''
    }

    inputs = {
    }

    constructor(props) {
        super(props);
        this.login = this.login.bind(this);
    }

    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow() {
        this.formWrapper.setNativeProps({
            style: {
                height: height + 150
            }
        });
        setTimeout(() => this.scrollView.scrollToEnd());
    }

    _keyboardDidHide() {
        this.formWrapper.setNativeProps({
            style: {
                height: height - 50
            }
        });
    }

    // Key press event handler
    login() {
        // Wrap the request into an action and then push it to message queue
        this.props.loginRequestAction({
            username: this.inputs.username.state.value,
            password: this.inputs.password.state.value
        });
    }

    // Event handler for hadling a change which happened in global state store and was mapped to the props
    componentWillReceiveProps(nextProps) {
        const succeeded = nextProps.loginReducer.loginResponseMap.get('succeeded');
        this.setState({
            loggedIn: succeeded,
            isMessageVisible: !succeeded,
            message: succeeded ? 'Login succeeded!' : 'Login failed.'
        }, () => {
            if (this.state.loggedIn)
                this.props.navigator.push({
                    screen: 'global.ProductList',
                    animated: true
                })
        });
    }

    renderMessage() {
        if (this.state.isMessageVisible)
            return (
                <Text style={ [Styles.message] }>
                  { this.state.message }
                </Text>
                );
        else
            return null;
    }

    render() {
        return (
            <View style={ Styles.container }>
              <ScrollView ref={ ref => (this.scrollView = ref) } contentContainerStyle={ { justifyContent: 'center', alignItems: 'center' } }>
                <View ref={ ref => (this.formWrapper = ref) } style={ Styles.form }>
                  <Text style={ [Styles.appTitle] }>Simplest App</Text>
                  <TextLineInput style={ [Styles.textLineInput] } ref={ ref => (this.inputs.username = ref) } minLength={ 5 } maxLength={ 20 } label="Username" placeholder="Enter username here"
                  />
                  <TextLineInput style={ [Styles.textLineInput] } ref={ ref => (this.inputs.password = ref) } minLength={ 5 } maxLength={ 20 } label="Password" placeholder="Enter password here"
                  />
                  { this.renderMessage() }
                  <Button textStyle={ [Styles.buttonText] } buttonStyle={ [Styles.button] } text='Login Now' onPress={ this.login } />
                </View>
              </ScrollView>
            </View>
        )
    }
}