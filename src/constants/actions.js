// All action types which are passed around components, reducers and sagas.

export const LOGIN_REQUEST='LOGIN_REQUEST';
export const LOGIN_RESPONSE='LOGIN_RESPONSE';
