import immutable from 'immutable';
import * as actionTypes from '../constants/actions';

// Action for wrapping the login request data object which includes the username and password.
export function loginRequestAction(request) {
    return {
        type: actionTypes.LOGIN_REQUEST,
        loginRequest: request
    };
}

// Action for wrapping response returned by server side or mocking function
export function loginResponseMapAction(response) {
    return {
        type: actionTypes.LOGIN_RESPONSE,
        loginResponseMap: immutable.fromJS(response)
    };
}
