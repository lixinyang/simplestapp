import { Navigation } from 'react-native-navigation';

import loginContainer from './containers/LoginContainer';
import productListContainer from './containers/ProductListContainer';

// Function for registering component containers, which are of navigatable screens, with global state store
// It will be called in App.js
const registerScreens = (store, Provider) => {
  Navigation.registerComponent('global.Login', () => loginContainer, store, Provider);
  Navigation.registerComponent('global.ProductList', () => productListContainer, store, Provider);
};

export default registerScreens;

